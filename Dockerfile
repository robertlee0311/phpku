FROM php:7.4-apache
ENV TZ="Asia/Jakarta"

# Install Extension
RUN apt-get update && \
    apt-get install -y libicu-dev zlib1g-dev libbz2-dev libffi-dev libpng-dev libzip-dev unzip nano && \
    docker-php-ext-install gd intl mysqli bz2 calendar exif ffi gettext pcntl sockets zip

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=2.7.4

# Enable Module
RUN a2enmod rewrite headers

RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf

# Apache
RUN service apache2 restart